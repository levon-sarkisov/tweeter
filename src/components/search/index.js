import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { searchTweetsAction } from "../../actions";
import { findHashTags } from "../../utils";

function Search() {
  const dispatch = useDispatch();
  const { tags } = useSelector(state => state.tags);

  const onSearch = e => {
    if (!tags) {
      return false;
    }

    const { value } = e.target;
    const result = findHashTags(value).filter(tag => tags[tag]);
    dispatch(searchTweetsAction(result));
  };

  return (
    <div>
      <p>
        <input type="text" className="form-control" onChange={onSearch} />
      </p>
    </div>
  );
}

export default Search;
