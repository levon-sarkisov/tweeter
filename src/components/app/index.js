import React from "react";
import Search from "../search";
import Tweet from "../tweet";
import Feed from "../feed";

function App() {
  return (
    <div className="app container">
      <Search />
      <Tweet />
      <Feed />
    </div>
  );
}

export default App;
