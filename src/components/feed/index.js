import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrashAlt, faPencilAlt } from "@fortawesome/free-solid-svg-icons";
import {
  editTweetAction,
  deleteTweetAction,
  editTagsAction
} from "../../actions";
import { findHashTags } from "../../utils";

function Feed() {
  const [activeTweet, setActiveTweet] = useState(null);
  const [newTweetValue, setNewTweetValue] = useState(null);
  const [selected, setSelected] = useState(null);

  const dispatch = useDispatch();
  const { tweets } = useSelector(state => state.tweets);
  const { search } = useSelector(state => state.tags);

  useEffect(() => {
    if (search) {
      setSelected([].concat.apply([], search).map(item => item.id));
    }
  }, [search]);

  const editTweet = tweet => {
    if (tweet.id === activeTweet) {
      const oldTags = findHashTags(tweet.value);

      const newTweet = tweet;
      newTweet.value = newTweetValue;

      const newTags = findHashTags(newTweet.value);

      dispatch(editTweetAction(newTweet));
      dispatch(editTagsAction({ oldTags, newTags, newTweet }));

      setActiveTweet(null);
      setNewTweetValue(null);
    } else {
      setActiveTweet(tweet.id);
    }
  };

  const deleteTweet = id => {
    dispatch(deleteTweetAction({ id }));
  };

  return (
    <div>
      <h1>Feed</h1>
      <ul className="feed">
        {tweets &&
          tweets
            .filter(tweet => {
              if (!selected || selected.length === 0) {
                return true;
              }
              return selected.indexOf(tweet.id) > -1;
            })
            .map(tweet => (
              <li
                id={tweet.id}
                key={tweet.id}
                className="alert alert-info feed--item"
              >
                <span
                  className="feed--item_edit"
                  onClick={() => editTweet(tweet)}
                >
                  <FontAwesomeIcon icon={faPencilAlt} />
                </span>
                <span
                  className="feed--item_delete"
                  onClick={() => deleteTweet(tweet.id)}
                >
                  <FontAwesomeIcon icon={faTrashAlt} />
                </span>
                {activeTweet !== tweet.id ? (
                  tweet.value
                ) : (
                  <textarea
                    value={newTweetValue || tweet.value}
                    onChange={e => setNewTweetValue(e.target.value)}
                  ></textarea>
                )}
              </li>
            ))}
      </ul>
    </div>
  );
}

export default Feed;
