import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { genId } from "../../utils";
import { addTweetAction, addTagsAction } from "../../actions";
import { findHashTags } from "../../utils";

function Tweet() {
  const [tweet, setTweet] = useState("");
  const [tags, setTags] = useState({});

  const dispatch = useDispatch();

  const onType = e => {
    const { value } = e.target;
    setTweet(value);
  };

  const saveTweet = () => {
    if (!tweet) {
      return false;
    }

    const newTweet = { id: genId(), value: tweet };

    findHashTags(tweet).forEach(item => {
      if (!tags[item]) {
        tags[item] = [newTweet];
      } else {
        tags[item].push(newTweet);
      }

      setTags(tags);
    });

    dispatch(addTweetAction(newTweet));
    dispatch(addTagsAction(tags));
  };

  return (
    <div>
      <p>
        <textarea className="form-control" rows="3" onKeyUp={onType} />
      </p>
      <p>
        <button
          className={`btn btn-primary ${!tweet ? "disabled" : ""}`}
          onClick={saveTweet}
        >
          send
        </button>
      </p>
    </div>
  );
}

export default Tweet;
