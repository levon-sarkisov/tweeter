export const ADD_TWEET = "ADD_TWEET";
export const ADD_TAGS = "ADD_TAGS";
export const EDIT_TWEET = "EDIT_TWEET";
export const DELETE_TWEET = "DELETE_TWEET";
export const EDIT_TAGS = "EDIT_TAGS";

export const SEARCH_TWEETS = "SEARCH_TWEETS";
