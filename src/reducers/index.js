import { combineReducers } from "redux";
import tweets from "./tweets";
import tags from "./tags";

const reducers = combineReducers({
  tweets,
  tags
});

export default reducers;
