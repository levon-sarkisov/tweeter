import * as type from "../../const";

export default function tags(state = {}, action) {
  switch (action.type) {
    case type.ADD_TAGS:
      return {
        tags: action.payload
      };
    case type.EDIT_TAGS:
      const { newTags, newTweet } = action.payload;

      let newObj = {};

      Object.keys(state.tags).forEach(name => {
        state.tags[name].forEach((obj, i) => {
          if (obj.id !== newTweet.id) {
            if (!newObj[name]) {
              newObj[name] = [];
            }
            newObj[name].push(state.tags[name][i]);
          }
        });

        newTags.forEach(tag => {
          if (!newObj[tag]) {
            newObj[tag] = [];
          }
          newObj[tag].push(newTweet);
        });
      });

      return {
        tags: newObj
      };
    case type.SEARCH_TWEETS:
      return {
        tags: state.tags,
        search: action.payload
          .filter(tag => state.tags[tag])
          .map(tag => state.tags[tag])
      };
    case type.DELETE_TWEET:
      return {
        tags: Object.keys(state.tags).map(name =>
          state.tags[name].filter(tag => tag.id !== action.payload.id)
        )
      };
    default:
      return state;
  }
}
