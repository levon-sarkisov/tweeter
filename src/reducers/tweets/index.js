import * as type from "../../const";

export default function tweets(state = [], action) {
  switch (action.type) {
    case type.ADD_TWEET:
      return {
        tweets: state.tweets
          ? [...state.tweets, action.payload]
          : [action.payload]
      };
    case type.EDIT_TWEET:
      return {
        tweets: state.tweets.map(tweet => {
          if (tweet.id === action.payload.id) {
            return action.payload;
          }
          return tweet;
        })
      };
    case type.DELETE_TWEET:
      return {
        tweets: state.tweets.filter(tweet => tweet.id !== action.payload.id)
      };
    default:
      return state;
  }
}
