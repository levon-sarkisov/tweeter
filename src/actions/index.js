import * as type from "../const";

export const addTweetAction = payload => ({
  type: type.ADD_TWEET,
  payload
});

export const addTagsAction = payload => ({
  type: type.ADD_TAGS,
  payload
});

export const editTweetAction = payload => ({
  type: type.EDIT_TWEET,
  payload
});

export const editTagsAction = payload => ({
  type: type.EDIT_TAGS,
  payload
});

export const deleteTweetAction = payload => ({
  type: type.DELETE_TWEET,
  payload
});

export const searchTweetsAction = payload => ({
  type: type.SEARCH_TWEETS,
  payload
});
