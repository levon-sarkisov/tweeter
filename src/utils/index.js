export const genId = () => Math.floor(Math.random(100000000) * 100000000);

export const stripHashTag = tag => tag.substr(1, tag.length - 1);

export const findHashTags = tweet =>
  Array.from(tweet.split(" ")).filter(tag =>
    tag.slice(0, 1) === "#" ? tag : null
  );
